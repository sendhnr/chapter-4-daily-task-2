const express = require('express');
const fs = require('fs')
const app = express();
const port = 3000;
const path = require('path');
const semuadata = require('./semuadata.js');

// menggunakan ejs
app.set('view engine', 'ejs');
app.set('views', path.join( __dirname, 'views'));

// memasang express static
app.set('views', path.join( __dirname, 'public'));
app.use('/public', express.static(path.join(__dirname, 'public')));

// menghubungkan ke index.ejs
app.get('/', (req, res) => {
    res.render('index');
});

// menghubungkan ke halaman data
app.get('/data1', (req, res) => {
    let varsatu =  JSON.stringify(semuadata)
    let vardua = JSON.parse(varsatu)
    let bungkus = []
    bungkus = vardua.filter((data)=>{
        return data.favoriteFruit == 'banana' && data.age <= 30 
    })
    res.send({
        bungkus
    });
});

app.get('/data2', (req, res) => {
    let varsatu =  JSON.stringify(semuadata)
    let vardua = JSON.parse(varsatu)
    let bungkus = []
    bungkus = vardua.filter((data)=>{
        return data.gender == 'female' && data.age >= 30 
    })
    res.send({
        bungkus
    });
});

app.get('/data3', (req, res) => {
    let varsatu =  JSON.stringify(semuadata)
    let vardua = JSON.parse(varsatu)
    let bungkus = []
    bungkus = vardua.filter((data)=>{
        return data.eyeColor == 'blue' && data.age >= 35 && favoriteFruit == 'apple'  
    })
    res.send({
        bungkus
    });
    // res.end('data tidak ditemukan');
});

app.get('/data4', (req, res) => {
    let varsatu =  JSON.stringify(semuadata)
    let vardua = JSON.parse(varsatu)
    let bungkus = []
    bungkus = vardua.filter((data)=>{
        return data.company == 'Pelangi' || data.company == 'Intel'  
    })
    res.send({
        bungkus
    })
});

app.get('/data5', (req, res) => {
    let varsatu =  JSON.stringify(semuadata)
    let vardua = JSON.parse(varsatu)
    let bungkus = []
    bungkus = vardua.filter((data)=>{
        let waktu = '2016-01-01 T 00:00:00 - 00:00'
        return data.registered <= waktu && data.isActive == true  
    })
    res.send({
        bungkus
    });
});

// menghubungkan ke halaman about
app.get('/about', (req, res) => {
    res.render('about');
});

// menghubungkan ke halaman 404
app.get('/404', (req, res) => {
    res.render('404');
});

// menghubungkan ke halaman 404 jika url tidak ada
app.use('/', (req, res) => {
    res.render('404');
    res.status(404);
});

app.listen(port, () => {
    console.log(`Example app listerning at http://localhost:${port}`)
});
